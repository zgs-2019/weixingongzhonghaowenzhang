"""
    辅助工具类，文章处理类
    2019-07-04 By luke
"""
import os, sys, re, json, requests
from bs4 import BeautifulSoup
import html5lib, hashlib, datetime, time
from urllib.request import urlretrieve
from pprint import pprint
from selenium import webdriver
from bs4 import BeautifulSoup
import html

Config = {
    'wxurl': 'http://mp.weixin.qq.com/profile?src=3&timestamp=1562290891&ver=1&signature=m-IYbwFdRYqzH1UGum1*GkDdYWWQj0IbnX5B3bwLyWg8UNoxdke6DTOPamRnoyiKjPoXfRsrATvvxwxTa2ySuw==',
    'path': './__recently/' + str(datetime.date.today()) + '/',
    'wxdomain': 'https://mp.weixin.qq.com'
}


class Handle(object):

    """ 文章数据处理 """

    __slots__ = ['origin', 'data', 'tools', 'path', 'html']

    def __init__(self, origin):
        self.origin = origin
        self.tools = Tools()
        self.data = {}
        self.path = Config['path'] + origin['app_msg_ext_info']['title'] + '/'
        pass

    """
        {
            'app_msg_ext_info': {
                'audio_fileid': 0,
                'author': '',
                'content': '',
                'content_url': '/s?timestamp=1562227159&amp;src=3&amp;ver=1&amp;signature=9d5Rk1NtfgCVLdafieUS4kFBSZZtDF6MEupIUwM5WPsGfTEuef7XUyJjYMaU5eRiGKpNPMDYuPOZGEwQBNLTM2XAthIZXif7Qb6c0UneNcuMmDS93g79vXYjbkBBaOVFB-9PFO1rg972ubc-86YtQ3siwXjwWKkWksK9MLti45A=',
                'copyright_stat': 0,
                'cover': 'http://mmbiz.qpic.cn/mmbiz_jpg/6Ndh2KgGAg3bB3WbXU2kiadMmKZa7PvMtHVJvsZZOpYmrVJOPEXpyeI9OdpEfDicHusPTcZEp18dtGwPg7BKRUaw/0',
                'del_flag': 1,
                'digest': '岡崎智弘',
                'duration': 0,
                'fileid': 0,
                'is_multi': 0,
                'item_show_type': 8,
                'malicious_content_type': 0,
                'malicious_title_reason_id': 0,
                'multi_app_msg_item_list': [],
                'play_url': '',
                'source_url': '',
                'subtype': 9,
                'title': '分享图片'
                },
            'comm_msg_info': {
                'content': '',
                'datetime': 1561123935,
                'fakeid': '3510927250',
                'id': 1000000271,
                'status': 2,
                'type': 49
            }
        }
    """
    def do(self):
        app_msg_ext_info = self.origin['app_msg_ext_info']
        comm_msg_info = self.origin['comm_msg_info']
        title = app_msg_ext_info['title']
        print('/******** 开始 [%s] *******/' % title)
        self.path = Config['path'] + '/data/' + title + '/'
        os.path.isdir(self.path) or os.mkdir(self.path)
        # title
        self.data['title'] = title
        # desc
        self.data['desc'] = app_msg_ext_info['digest']
        # cover
        self.data['cover'] = self.tools.download_remote_image(app_msg_ext_info['cover'], Config['path']+'imgs/cover/')
        # created_at
        self.data['created_at'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(comm_msg_info['datetime']))
        # 请求
        self.request(Config['wxdomain'] + app_msg_ext_info['content_url'])
        # 解析HTML
        self.parser_html()
        self.tools.write_json(self.data, self.path + 'data.json')
        #
        print('/******** 结束 [%s] *******/' % title)
        pass

    def request(self, url):
        url = self.tools.html_parser(url)
        print('     解析路由[%s]' % url)
        browser = webdriver.Chrome(executable_path=os.path.abspath("chromedriver"))
        browser.get(url)
        browser.execute_script('window.scrollTo(0, document.body.scrollHeight)')
        time.sleep(3)
        self.html = browser.page_source
        browser.close()
        self.write_html()
        pass

    def parser_html(self):
        bs = BeautifulSoup(self.html, 'html5lib')
        content = bs.find('div', id='js_content')
        # view
        self.data['view'] = bs.find('span', id='readNum3').get_text('', strip=True)
        # TODO 删除底部
        a = content.find('p', text=re.compile('ABOUT US'))
        if a:
            a.parent.parent.parent.parent.extract()
            pass
        # TODO 去除头部
        if content.find('section'):
            secs = [sec for sec in content.find('section').contents if sec.name == 'section']
            for sec in secs[:2]:
                sec.extract()
                pass
            # desc
            if len(secs) > 2 and secs[3]:
                self.data['desc'] = secs[3].get_text('', strip=True)
                pass
            pass
        # TODO 图片路径
        for img in content.find_all('img'):
            if img.has_attr('data-src'):
                src = img.attrs['data-src']
            else:
                src = img.attrs['src']
                pass
            new_src = '__IMAGE_PATH__/' + self.tools.download_remote_image(src, Config['path'] + 'imgs/')
            img.attrs['src'] = new_src
            img.attrs['data-src'] = new_src
            # 删除属性
            del img['crossorigin']
            pass
        # TODO content
        self.data['content'] = content.prettify()
        self.write_content()
        pass

    """
        写入index html
    """
    def write_html(self):
        with open(self.path + 'index.html', 'w') as i:
            i.write(self.html)
            pass
        pass

    """
        写入content html
    """
    def write_content(self):
        with open(self.path + 'content.html', 'w') as i:
            i.write(self.data['content'])
            pass
        pass

    pass


class Tools(object):

    """ 工具类 """

    """
        请求微信搜狗列表
    """
    def request_weixin_sogou(self):
        with requests.get(Config['wxurl']) as r:
            return r.content
            pass
        pass

    """
        获取最近发布的文章列表(10条)
    """
    def fetch_recently_msg_list(self):
        data_filepath = Config['path'] + 'list.json'
        if os.path.isfile(data_filepath):
            return json.load(open(data_filepath, 'r'))
            pass
        os.path.isdir(Config['path']) or os.mkdir(Config['path'])
        remote_response = self.request_weixin_sogou()
        bs = BeautifulSoup(remote_response, 'html5lib')
        pattern = re.compile(r"var msgList = (.*?);$", re.MULTILINE | re.DOTALL)
        script = bs.find('script', text=pattern)
        msg_list = pattern.search(script.text).group(1)
        msg_list = json.loads(msg_list)['list']
        self.write_json(msg_list, data_filepath)
        return msg_list
        pass

    """
        写入json文件
    """
    def write_json(self, data, filename):
        with open(filename, 'w') as d:
            json_data = json.dumps(data, indent=4, ensure_ascii=False)
            d.write(json_data)
            pass
        pass

    """ 
        下载远程图片
    """
    def download_remote_image(self, url, path):
        os.path.isdir(path) or os.makedirs(path)
        if len(url.split('wx_fmt=')) == 2:
            extension = url.split('wx_fmt=')[1]
        else:
            extension = 'jpeg'
            pass
        filename = self.md5_encode(url) + '.' + extension
        filepath = path + filename
        if not os.path.isfile(filepath):
            urlretrieve(url, filepath)
            pass
        return filename
        pass

    """
        md5加密
    """
    def md5_encode(self, data):
        return hashlib.md5(data.encode('utf8')).hexdigest()
        pass

    """
        转义
    """
    def html_parser(self, data):
        return html.unescape(data)
        pass

    """
        转换时间
    """
    def turn_date(self, d):
        if '-' in d:
            return d
        g = re.match(r'(\d{1,2})月(\d{1,2})日', d)
        if g:
            return '2019-' + g.group(1).zfill(2) + '-' + g.group(2).zfill(2)
        return ''
        pass

    pass


