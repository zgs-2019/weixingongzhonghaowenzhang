"""

"""

import requests
from bs4 import BeautifulSoup

url = 'https://learnku.com/docs/laravel/5.8/validation/3899'

with open('./val.htm') as r:
    bs = BeautifulSoup(r.read(), 'html.parser')
    bs_body     =   bs.find('div', attrs={'class': 'markdown-body'})
    h4s     =   bs_body.find_all('h4')
    print(h4s)
